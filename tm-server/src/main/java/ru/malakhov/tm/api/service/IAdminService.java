package ru.malakhov.tm.api.service;

import java.io.IOException;

public interface IAdminService {

    void saveDataBinary() throws Exception;

    void loadDataBinary() throws Exception;

    void clearDataBinary() throws IOException;

    void saveDataBase64() throws Exception;

    void loadDataBase64() throws Exception;

    void clearDataBase64() throws IOException;

    void saveDataJson() throws Exception;

    void loadDataJson() throws Exception;

    void clearDataJson() throws IOException;

    void saveDataXml() throws Exception;

    void loadDataXml() throws Exception;

    void clearDataXml() throws IOException;

}