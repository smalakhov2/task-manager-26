package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IAdminUserEndpoint {

    @WebMethod
    @NotNull
    Result clearAllUser(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    List<UserDto> getAllUserList(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result lockUserByLogin(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "login") @Nullable String login
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result unlockUserByLogin(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "login") @Nullable String login
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Result removeUserByLogin(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "login") @Nullable String login
    ) throws AbstractException;

}