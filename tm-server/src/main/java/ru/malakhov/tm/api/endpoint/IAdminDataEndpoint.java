package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAdminDataEndpoint extends IEndpoint {

    @NotNull
    @WebMethod
    Result saveDataBinary(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result loadDataBinary(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result clearDataBinary(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result saveDataBase64(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result loadDataBase64(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result clearDataBase64(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result saveDataJson(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result loadDataJson(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result clearDataJson(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result saveDataXml(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result loadDataXml(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result clearDataXml(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

}