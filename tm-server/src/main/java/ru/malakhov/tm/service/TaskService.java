package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.ISqlSessionProvider;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IndexIncorrectException;
import ru.malakhov.tm.repository.TaskRepository;

import java.util.List;

public final class TaskService extends AbstractService<TaskDto, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ISqlSessionProvider sqlSessionProvider) {
        super(sqlSessionProvider);
    }

    @Override
    public @NotNull ITaskRepository getRepository() {
        return new TaskRepository(getEntityManager());
    }
    
    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        
        @NotNull final TaskDto task = new TaskDto(name, "", userId);
        persist(task);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        
        @NotNull final TaskDto task = new TaskDto(name, description, userId);
        persist(task);
    }

    @NotNull
    @Override
    public List<TaskDto> findAllDto() {
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findAllDto();
    }

    @NotNull
    @Override
    public List<Task> findAllEntity() {
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findAllEntity();
    }

    @NotNull
    public List<TaskDto> findAllDtoByUserId(
            @Nullable final String userId
    ) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findAllDtoByUserId(userId);
    }

    @NotNull
    public List<Task> findAllEntityByUserId(
            @Nullable final String userId
    ) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findAllEntityByUserId(userId);
    }

    @Nullable
    @Override
    public TaskDto findOneDtoById(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findOneDtoById(id);
    }

    @Nullable
    @Override
    public Task findOneEntityById(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findOneEntityById(id);
    }

    @Nullable
    @Override
    public TaskDto findOneDtoById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findOneDtoById(userId, id);
    }

    @Nullable
    @Override
    public Task findOneEntityById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findOneEntityById(userId, id);
    }

    @Nullable
    @Override
    public TaskDto findOneDtoByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findOneDtoByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task findOneEntityByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findOneEntityByIndex(userId, index);
    }

    @Nullable
    @Override
    public TaskDto findOneDtoByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findOneDtoByName(userId, name);
    }

    @Nullable
    @Override
    public Task findOneEntityByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findOneEntityByName(userId, name);
    }

    @Override
    public void removeAll() {
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll();
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAllByUserId(userId);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(id);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(userId, id);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();

        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneByIndex(userId, index);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneByName(userId, name);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final TaskDto task = findOneDtoById(userId, id);
        if (task == null) return;
        task.setName(name);
        task.setDescription(description);
        merge(task);
    }

    @Override
    public void updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final TaskDto task = findOneDtoByIndex(userId, index);
        if (task == null) return;
        task.setName(name);
        task.setDescription(description);
        merge(task);
    }

}
