package ru.malakhov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.service.IAdminService;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.Domain;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.file.Files;

public final class AdminService implements IAdminService {

    @NotNull
    private final String FILE_BINARY_PATH = "./data.binary";

    @NotNull
    private final String FILE_BASE64_PATH = "./data.base64";

    @NotNull
    private final String FILE_JSON_PATH = "./data.json";

    @NotNull
    private final String FILE_XML_PATH = "./data.xml";

    @NotNull
    private final IServiceLocator serviceLocator;

    public AdminService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void saveDataBinary() throws Exception {
        @NotNull final File file = new File(FILE_BINARY_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().saveData(domain);
        try (
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
                @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)
        ) {
            objectOutputStream.writeObject(domain);
        }
    }

    @Override
    public void loadDataBinary() throws Exception {
        try (
                @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY_PATH);
                @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            serviceLocator.getDomainService().loadData(domain);
        }
    }

    @Override
    public void clearDataBinary() throws IOException {
        @NotNull final File file = new File(FILE_BINARY_PATH);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void saveDataBase64() throws Exception {
        @NotNull final File file = new File(FILE_BASE64_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().saveData(domain);
        try (
                @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)
        ) {
            objectOutputStream.writeObject(domain);
            final byte[] bytes = byteArrayOutputStream.toByteArray();
            @NotNull final String base64 = new BASE64Encoder().encode(bytes);
            fileOutputStream.write(base64.getBytes());
        }
    }

    @Override
    public void loadDataBase64() throws Exception {
        final byte[] bytes64;
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BASE64_PATH)) {
            bytes64 = new BASE64Decoder().decodeBuffer(fileInputStream);
        }
        try (
                @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes64);
                @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)
        ) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            serviceLocator.getDomainService().loadData(domain);
        }
    }

    @Override
    public void clearDataBase64() throws IOException {
        @NotNull final File file = new File(FILE_BASE64_PATH);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void saveDataJson() throws Exception {
        @NotNull final File file = new File(FILE_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().saveData(domain);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(json.getBytes());
        }
    }

    @Override
    public void loadDataJson() throws Exception {
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_JSON_PATH)) {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
            serviceLocator.getDomainService().loadData(domain);
        }
    }

    @Override
    public void clearDataJson() throws IOException {
        @NotNull final File file = new File(FILE_JSON_PATH);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void saveDataXml() throws Exception {
        @NotNull final File file = new File(FILE_XML_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().saveData(domain);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(json.getBytes());
        }
    }

    @Override
    public void loadDataXml() throws Exception {
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_XML_PATH)) {
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
            serviceLocator.getDomainService().loadData(domain);
        }
    }

    @Override
    public void clearDataXml() throws IOException {
        @NotNull final File file = new File(FILE_XML_PATH);
        Files.deleteIfExists(file.toPath());
    }

}
