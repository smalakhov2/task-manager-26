package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.exception.system.PropertyLoadException;

import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private final String NAME = "/application.properties";

    @NotNull
    private final Properties properties = new Properties();

    @Override
    public void init() throws Exception {
        try (@Nullable final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME)) {
            if (inputStream == null) throw new PropertyLoadException();
            properties.load(inputStream);
        }
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String propertyHost = properties.getProperty("server.host", "localhost");
        @Nullable final String envHost = System.getProperty("server.host");
        @NotNull final String value = (envHost == null) ? propertyHost : envHost;
        return value;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String propertyPort = properties.getProperty("server.port", "80");
        @Nullable final String envPort = System.getProperty("server.port");
        @NotNull final String value = (envPort == null) ? propertyPort : envPort;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        @NotNull final String propertySalt = properties.getProperty("session.salt", "sfsdf5");
        @Nullable final String envSalt = System.getProperty("session.salt");
        @NotNull final String value = (envSalt == null) ? propertySalt : envSalt;
        return value;
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        @NotNull final String propertyCycle = properties.getProperty("session.cycle", "3435");
        @Nullable final String envCycle = System.getProperty("session.cycle");
        @NotNull final String value = (envCycle == null) ? propertyCycle : envCycle;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        @NotNull final String propertyJdbcDriver
                = properties.getProperty("jdbc.driver", "com.mysql.jdbc.Driver");
        @Nullable final String envJdbcDriver = System.getProperty("jdbc.driver");
        @NotNull final String value = (envJdbcDriver == null) ? propertyJdbcDriver : envJdbcDriver;
        return value;
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        @NotNull final String propertyJdbcUrl =
                properties.getProperty("jdbc.url", "jdbc:mysql://localhost:3306/task-manager?zeroDateTimeBehavior=convertToNull");
        @Nullable final String envJdbcUrl = System.getProperty("jdbc.url");
        @NotNull final String value = (envJdbcUrl == null) ? propertyJdbcUrl : envJdbcUrl;
        return value;
    }

    @NotNull
    @Override
    public String getJdbcUsername() {
        @NotNull final String propertyJdbcUsername = properties.getProperty("jdbc.username", "root");
        @Nullable final String envJdbcUsername = System.getProperty("jdbc.username");
        @NotNull final String value = (envJdbcUsername == null) ? propertyJdbcUsername : envJdbcUsername;
        return value;
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        @NotNull final String propertyJdbcPassword = properties.getProperty("jdbc.password", "root");
        @Nullable final String envJdbcPassword = System.getProperty("jdbc.password");
        @NotNull final String value = (envJdbcPassword == null) ? propertyJdbcPassword : envJdbcPassword;
        return value;
    }

}
