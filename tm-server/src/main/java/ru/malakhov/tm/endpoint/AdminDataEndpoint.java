package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.IAdminDataEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.response.Fail;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.dto.response.Success;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    public AdminDataEndpoint() {
        super(null);
    }

    public AdminDataEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataBinary(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getAdminService().saveDataBinary();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataBinary(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadDataBinary();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataBinary(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearDataBinary();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataBase64(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveDataBase64();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataBase64(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadDataBase64();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataBase64(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearDataBase64();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataJson(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveDataJson();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataJson(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadDataJson();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataJson(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearDataJson();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataXml(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveDataXml();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataXml(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadDataXml();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataXml(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearDataXml();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

}