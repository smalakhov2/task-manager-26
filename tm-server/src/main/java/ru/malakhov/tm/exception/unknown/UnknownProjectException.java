package ru.malakhov.tm.exception.unknown;

import ru.malakhov.tm.exception.AbstractException;

public final class UnknownProjectException extends AbstractException {

    public UnknownProjectException() {
        super("Error! Unknown project ...");
    }

}
