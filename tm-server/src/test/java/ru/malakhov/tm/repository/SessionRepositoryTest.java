package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.entity.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class SessionRepositoryTest extends AbstractDataTest {

    @NotNull
    private final SessionDto sessionOne = bootstrap.getSessionService().setSignature(
                    new SessionDto(
                            System.currentTimeMillis(),
                            userDto.getId(),
                            null
                    )
    );

    @NotNull
    private final SessionDto sessionTwo = bootstrap.getSessionService().setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    userDto.getId(),
                    null)
    );

    @NotNull
    private final SessionDto sessionThree = bootstrap.getSessionService().setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    adminDto.getId(),
                    null)
    );

    @NotNull
    private final SessionDto sessionFour = bootstrap.getSessionService().setSignature(
                    new SessionDto(
                            System.currentTimeMillis(),
                            adminDto.getId(),
                            null)
    );

    @NotNull
    private final SessionDto unknownSession = bootstrap.getSessionService().setSignature(
            new SessionDto(System.currentTimeMillis(),
                    unknownUserDto.getId(),
                    null)
    );

    @NotNull
    private final List<SessionDto> userSessions = new ArrayList<>(Arrays.asList(sessionOne, sessionTwo));

    @NotNull
    private final List<SessionDto> adminSessions = new ArrayList<>(Arrays.asList(sessionThree, sessionFour));

    @NotNull
    private ISessionRepository getRepository() {
        return bootstrap.getSessionService().getRepository();
    }

    public SessionRepositoryTest() throws Exception {
        super();
    }

    @Before
    public void before() {
        bootstrap.getUserService().persist(userDto, adminDto);
        bootstrap.getSessionService().persist(userSessions);
        bootstrap.getSessionService().persist(adminSessions);
    }

    @After
    public void after() {
        bootstrap.getSessionService().removeAll();
        bootstrap.getUserService().removeOne(userDto);
        bootstrap.getUserService().removeOne(adminDto);
    }

    @Test
    public void testFindAllDto() {
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final List<SessionDto> sessions = repository.findAllDto();
        Assert.assertEquals(4, sessions.size());
    }

    @Test
    public void testFindAllEntity() {
        @NotNull final ISessionRepository repository = getRepository();
        @NotNull final List<Session> sessions = repository.findAllEntity();
        Assert.assertEquals(4, sessions.size());
    }

    @Test
    public void testFindAllDtoByUserId() {
        @NotNull final ISessionRepository repository = getRepository();

        @NotNull final List<SessionDto> sessions = repository.findAllDtoByUserId(userDto.getId());
        Assert.assertEquals(2, sessions.size());

        @NotNull final List<SessionDto> emptyList = repository.findAllDtoByUserId(unknownUserDto.getId());
        Assert.assertEquals(0, emptyList.size());
    }

    @Test
    public void testFindAllEntityByUserId() {
        @NotNull final ISessionRepository repository = getRepository();

        @NotNull final List<Session> sessions = repository.findAllEntityByUserId(userDto.getId());
        Assert.assertEquals(2, sessions.size());

        @NotNull final List<Session> emptyList = repository.findAllEntityByUserId(unknownUserDto.getId());
        Assert.assertEquals(0, emptyList.size());
    }

    @Test
    public void testFindOneDtoById() {
        @NotNull final ISessionRepository repository = getRepository();

        @Nullable final SessionDto session = repository.findOneDtoById(sessionOne.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(session, sessionOne);

        @Nullable final SessionDto unknown = repository.findOneDtoById(unknownSession.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityById() {
        @NotNull final ISessionRepository repository = getRepository();

        @Nullable final Session session = repository.findOneEntityById(sessionOne.getId());
        Assert.assertNotNull(session);

        @Nullable final Session unknown = repository.findOneEntityById(unknownSession.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testRemoveAll() {
        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll();
            repository.commit();
            @NotNull final List<SessionDto> sessions = repository.findAllDto();
            Assert.assertEquals(0, sessions.size());
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveAllByUserId() {
        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAllByUserId(unknownUserDto.getId());
            repository.commit();
            @NotNull List<SessionDto> sessions = repository.findAllDto();
            Assert.assertEquals(4, sessions.size());

            repository.begin();
            repository.removeAllByUserId(userDto.getId());
            repository.commit();
            sessions = repository.findAllDto();
            Assert.assertEquals(2, sessions.size());
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(unknownSession.getId());
            repository.commit();
            @NotNull List<SessionDto> sessions = repository.findAllDto();
            Assert.assertEquals(4, sessions.size());

            repository.begin();
            repository.removeOneById(sessionOne.getId());
            repository.commit();
            @Nullable final SessionDto session = repository.findOneDtoById(sessionOne.getId());
            Assert.assertNull(session);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}
