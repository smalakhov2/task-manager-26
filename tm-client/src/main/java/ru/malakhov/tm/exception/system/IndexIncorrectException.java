package ru.malakhov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.exception.AbstractException;

public final class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException(@NotNull final String value, @NotNull final Throwable cause) {
        super("Error! This value ``" + value + "`` is not number", cause);
    }

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}