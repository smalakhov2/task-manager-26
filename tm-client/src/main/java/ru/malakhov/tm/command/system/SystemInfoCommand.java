package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.util.SystemUtil;

public final class SystemInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "system-info";
    }

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String description() {
        return "Display information about system.";
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        SystemUtil.showSystemInfo();
    }

    @Override
    public boolean secure() {
        return false;
    }

}