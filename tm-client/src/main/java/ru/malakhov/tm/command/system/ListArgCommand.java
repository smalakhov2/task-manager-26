package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.Set;

public final class ListArgCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Set<String> commandsArg = serviceLocator.getCommandService().getCommandsArg();
        for (@NotNull final String arg : commandsArg) System.out.println(arg);
    }

    @Override
    public boolean secure() {
        return false;
    }

}