package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.InformationConst;

public class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(InformationConst.VERSION);
    }

    @Override
    public boolean secure() {
        return false;
    }

}