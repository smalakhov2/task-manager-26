package ru.malakhov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.TaskDto;
import ru.malakhov.tm.util.TerminalUtil;

public final class TaskShowByNameCommand extends AbstractTaskShowCommand {

    @NotNull
    @Override
    public String name() {
        return "task-show-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final SessionDto session = serviceLocator.getPropertyService().getSession();
        @Nullable final TaskDto task = serviceLocator.getTaskEndpoint().getTaskByName(session, name);
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}