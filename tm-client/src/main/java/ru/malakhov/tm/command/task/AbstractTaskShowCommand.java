package ru.malakhov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.TaskDto;

public abstract class AbstractTaskShowCommand extends AbstractCommand {

    protected void showTask(@Nullable final TaskDto task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

}