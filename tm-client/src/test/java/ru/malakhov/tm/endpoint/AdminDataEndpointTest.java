package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.category.IntegrationCategory;

import java.util.List;

@Category(IntegrationCategory.class)
public final class AdminDataEndpointTest extends AbstractIntegrationTest {

    @NotNull
    private final AdminDataEndpoint adEndpoint = new AdminDataEndpointService().getAdminDataEndpointPort();

    @NotNull
    private final AdminUserEndpoint auEndpoint = new AdminUserEndpointService().getAdminUserEndpointPort();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    private final int PROJECT_COUNT = 3;

    private final int TASK_COUNT = 3;

    private final int USER_COUNT = 3;

    private final int SESSION_COUNT = 3;

    @NotNull
    private final String ADMIN_LOGIN = "admin";

    @NotNull
    private final String ADMIN_PASSWORD = "admin";

    @Nullable
    private SessionDto testUserSession;

    @Before
    public void before() throws AbstractException_Exception {
        projectEndpoint.createProject(userSession, "Project1", "");
        projectEndpoint.createProject(userSession, "Project2", "");
        projectEndpoint.createProject(adminSession, "AdmProject3", "");
        taskEndpoint.createTask(userSession, "Task1", "");
        taskEndpoint.createTask(userSession, "Task2", "");
        taskEndpoint.createTask(adminSession, "AdmTask3", "");
        userEndpoint.registryUser("user", "user", "user@user.ru");
        testUserSession = sessionEndpoint.openSession("user", "user");
    }

    @After
    public void after() throws AbstractException_Exception {
        taskEndpoint.clearAllTask(adminSession);
        projectEndpoint.clearAllProject(adminSession);
        auEndpoint.removeUserByLogin(adminSession, "user");
        adEndpoint.clearDataBinary(adminSession);
        adEndpoint.clearDataBase64(adminSession);
        adEndpoint.clearDataJson(adminSession);
        adEndpoint.clearDataXml(adminSession);

    }

    @Test
    public void testSaveDataBinary() throws AbstractException_Exception {
        @NotNull final Result result =  adEndpoint.saveDataBinary(adminSession);
        Assert.assertTrue(result.success);
    }

    @Test
    public void testLoadDataBinary() throws AbstractException_Exception {
        adEndpoint.saveDataBinary(adminSession);
        @Nullable final SessionDto reopenAdminSession = sessionEndpoint.openSession("admin", "admin");
        @NotNull final Result result = adEndpoint.loadDataBinary(reopenAdminSession);
        sessionEndpoint.closeSession(reopenAdminSession);
        Assert.assertTrue(result.success);
        @NotNull final List<ProjectDto> projects = projectEndpoint.getAllProjectList(adminSession);
        Assert.assertEquals(PROJECT_COUNT, projects.size());
        @NotNull final List<TaskDto> tasks = taskEndpoint.getAllTaskList(adminSession);
        Assert.assertEquals(TASK_COUNT, tasks.size());
        @NotNull final List<UserDto> users = auEndpoint.getAllUserList(adminSession);
        Assert.assertEquals(USER_COUNT, users.size());
        @NotNull final List<SessionDto> sessions = sessionEndpoint.getAllSessionList(adminSession);
        Assert.assertEquals(SESSION_COUNT, sessions.size());
    }

    @Test
    public void testClearDataBinary() throws AbstractException_Exception {
        adEndpoint.saveDataBinary(adminSession);
        @NotNull final Result result = adEndpoint.clearDataBinary(adminSession);
        Assert.assertTrue(result.success);
    }

    @Test
    public void testSaveDataBase64() throws AbstractException_Exception {
        @NotNull final Result result =  adEndpoint.saveDataBase64(adminSession);
        Assert.assertTrue(result.success);
    }

    @Test
    public void testLoadDataDataBase64() throws AbstractException_Exception {
        adEndpoint.saveDataBase64(adminSession);
        @Nullable final SessionDto reopenAdminSession = sessionEndpoint.openSession("admin", "admin");
        @NotNull final Result result = adEndpoint.loadDataBase64(reopenAdminSession);
        sessionEndpoint.closeSession(reopenAdminSession);
        Assert.assertTrue(result.success);
        @NotNull final List<ProjectDto> projects = projectEndpoint.getAllProjectList(adminSession);
        Assert.assertEquals(PROJECT_COUNT, projects.size());
        @NotNull final List<TaskDto> tasks = taskEndpoint.getAllTaskList(adminSession);
        Assert.assertEquals(TASK_COUNT, tasks.size());
        @NotNull final List<UserDto> users = auEndpoint.getAllUserList(adminSession);
        Assert.assertEquals(USER_COUNT, users.size());
        @NotNull final List<SessionDto> sessions = sessionEndpoint.getAllSessionList(adminSession);
        Assert.assertEquals(SESSION_COUNT, sessions.size());
    }

    @Test
    public void testClearDataBase64() throws AbstractException_Exception {
        adEndpoint.saveDataBase64(adminSession);
        @NotNull final Result result = adEndpoint.clearDataBase64(adminSession);
        Assert.assertTrue(result.success);
    }

    @Test
    public void testSaveDataJson() throws AbstractException_Exception {
        @NotNull final Result result =  adEndpoint.saveDataJson(adminSession);
        Assert.assertTrue(result.success);
    }

    @Test
    public void testLoadDataDataJson() throws AbstractException_Exception {
        adEndpoint.saveDataJson(adminSession);
        @Nullable final SessionDto reopenAdminSession = sessionEndpoint.openSession("admin", "admin");
        @NotNull final Result result = adEndpoint.loadDataJson(reopenAdminSession);
        sessionEndpoint.closeSession(reopenAdminSession);
        Assert.assertTrue(result.success);
        @NotNull final List<ProjectDto> projects = projectEndpoint.getAllProjectList(adminSession);
        Assert.assertEquals(PROJECT_COUNT, projects.size());
        @NotNull final List<TaskDto> tasks = taskEndpoint.getAllTaskList(adminSession);
        Assert.assertEquals(TASK_COUNT, tasks.size());
        @NotNull final List<UserDto> users = auEndpoint.getAllUserList(adminSession);
        Assert.assertEquals(USER_COUNT, users.size());
        @NotNull final List<SessionDto> sessions = sessionEndpoint.getAllSessionList(adminSession);
        Assert.assertEquals(SESSION_COUNT, sessions.size());
    }

    @Test
    public void testClearDataJson() throws AbstractException_Exception {
        adEndpoint.saveDataJson(adminSession);
        @NotNull final Result result = adEndpoint.clearDataJson(adminSession);
        Assert.assertTrue(result.success);
    }

    @Test
    public void testSaveDataXml() throws AbstractException_Exception {
        @NotNull final Result result =  adEndpoint.saveDataXml(adminSession);
        Assert.assertTrue(result.success);
    }

    @Test
    public void testLoadDataDataXml() throws AbstractException_Exception {
        adEndpoint.saveDataXml(adminSession);
        @Nullable final SessionDto reopenAdminSession = sessionEndpoint.openSession("admin", "admin");
        @NotNull final Result result = adEndpoint.loadDataXml(reopenAdminSession);
        sessionEndpoint.closeSession(reopenAdminSession);
        Assert.assertTrue(result.success);
        @NotNull final List<ProjectDto> projects = projectEndpoint.getAllProjectList(adminSession);
        Assert.assertEquals(PROJECT_COUNT, projects.size());
        @NotNull final List<TaskDto> tasks = taskEndpoint.getAllTaskList(adminSession);
        Assert.assertEquals(TASK_COUNT, tasks.size());
        @NotNull final List<UserDto> users = auEndpoint.getAllUserList(adminSession);
        Assert.assertEquals(USER_COUNT, users.size());
        @NotNull final List<SessionDto> sessions = sessionEndpoint.getAllSessionList(adminSession);
        Assert.assertEquals(SESSION_COUNT, sessions.size());
    }

    @Test
    public void testClearDataXml() throws AbstractException_Exception {
        adEndpoint.saveDataXml(adminSession);
        @NotNull final Result result = adEndpoint.clearDataXml(adminSession);
        Assert.assertTrue(result.success);
    }

}